﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class ShotProperties
    {
        public float Angle { get; private set; }
        public float Force { get; private set; }

        public ShotProperties(float angle, float force)
        {
            Angle = angle;
            Force = force;
        }
    }
}

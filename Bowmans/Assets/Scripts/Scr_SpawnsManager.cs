using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class  Scr_SpawnsManager : Manager
{
    [SerializeField] public List<GameObject> SpawnPoints;

    void Start()
    {
        SpawnPoints = GameObject.FindGameObjectsWithTag("Respawn").ToList();
        SpawnPoints = SpawnPoints.OrderBy(n => n.name).ToList();
    }

    public void MoveToSpawn(int index, Transform player)
    {
        player.position = SpawnPoints[index].transform.position;
        player.rotation = SpawnPoints[index].transform.rotation;
        player.localScale = SpawnPoints[index].transform.localScale;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scr_BarScript : MonoBehaviour
{
    [SerializeField] public Slider slider;
    void Start()
    {
        slider = GetComponent<Slider>();
    }
    public void SetMaxFill(float amount)
    {
        slider.maxValue = amount;
        slider.value = amount;
    }
    public void SetFill(float amount)
    {
        slider.value = amount;
    }
}

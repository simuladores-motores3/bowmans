using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Scr_CameraMovement : MonoBehaviour
{
    [SerializeField] CinemachineBrain brain;

    void Start()
    {
        brain = gameObject.GetComponent<CinemachineBrain>();
    }

    public void SetCamera(CinemachineVirtualCamera newVCam)
    {
        brain.ActiveVirtualCamera.Priority -=1;
        newVCam.Priority += 1;
    }
}

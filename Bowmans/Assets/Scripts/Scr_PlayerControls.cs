using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;
using Cinemachine;

public class Scr_PlayerControls : MonoBehaviour
{
    Scr_GameManager gm;
    Scr_LineManager lineManager;

    [SerializeField] GameObject arrowSpawn;
    ShotProperties currentShot;

    private void Awake()
    {
        gm = GameObject.Find("Game Manager").GetComponent<Scr_GameManager>();
        lineManager = gm.gameObject.GetComponent<Scr_LineManager>();
        foreach (Transform t in transform) { arrowSpawn = (t.name == "ArrowSpawn") ? t.gameObject : null; };
    }

    void Update()
    {
        if (gm.inGame)
        {
            //Start Drawing Line
            if (Input.GetMouseButtonDown(0)) { lineManager.StartDrawingLine(); }
            //Preview Line
            else if (Input.GetMouseButton(0)) { lineManager.PreviewLine(); }
            //End Drawing Line and Shoot
            else if (Input.GetMouseButtonUp(0))
            {
                currentShot = lineManager.EndDrawingLine();
                if (currentShot != null)
                {
                    Shoot(currentShot);
                }
            }
        }
    }

    private void Shoot(ShotProperties shot)
    {
        SetArrowSpawn();
        gm.Shoot(shot, arrowSpawn);
    }
    private void SetArrowSpawn()
    {
        arrowSpawn.transform.rotation = Quaternion.Euler(0f, 0f, currentShot.Angle);
    }

}

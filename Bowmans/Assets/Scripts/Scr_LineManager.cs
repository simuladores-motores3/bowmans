using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts;

public class Scr_LineManager : Manager
{
    Vector3 startPosition;
    Vector3 lastPosition;
    Vector2 a;
    Vector2 b;
    GameObject currentLineObject;
    LineRenderer currentLineRenderer;
    public Material lineMaterial;
    public float lineThickness = 0.25f;
    public Canvas parentCanvas;
    public Shader shader;
    public Color color;
    public float lineScalar = 0.25f;

    public float shotPowerModifier = 20f;
    public float currentShotPower = 0f;
    public float currentShotAngle = 0f;

    private void Awake()
    {
        parentCanvas = FindObjectOfType<Canvas>();
    }

    Vector3 GetMousePosition()
    {
        Vector2 movePos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            parentCanvas.transform as RectTransform,
            Input.mousePosition, parentCanvas.worldCamera,
            out movePos);
        Vector3 positionToReturn = parentCanvas.transform.TransformPoint(movePos);
        positionToReturn.z = parentCanvas.transform.position.z - 0.01f;
        return positionToReturn;
    }
    public void StartDrawingLine()
    {
        startPosition = GetMousePosition();
        lastPosition = GetMousePosition();
        currentLineObject = new GameObject();
        currentLineObject.transform.position = startPosition;
        currentLineRenderer = currentLineObject.AddComponent<LineRenderer>();
        currentLineRenderer.SetPosition(0, startPosition);
        currentLineRenderer.SetPosition(1, startPosition);
        currentLineRenderer.material.shader = shader;
        currentLineRenderer.material = lineMaterial;
        currentLineRenderer.material.color = color;
        currentLineRenderer.startWidth = lineThickness;
        currentLineRenderer.endWidth = lineThickness;
    }
    public void PreviewLine()
    {
        a = new Vector2(currentLineRenderer.GetPosition(0).x, currentLineRenderer.GetPosition(0).y);
        b = new Vector2(GetMousePosition().x, GetMousePosition().y);
        if (Vector3.Distance(b, a) * lineScalar > lineScalar && Vector3.Distance(b, a) * lineScalar < 10*lineScalar)
        {
            currentShotPower = Vector3.Distance(b, a) * lineScalar;
            if ((CalculateAngle(new Vector2(-a.x, a.y), new Vector2(-b.x, b.y)) < 90) || (CalculateAngle(a, b) < 90))
            {
                CalculateAngle(a, b);
                lastPosition = GetMousePosition();
                currentLineRenderer.SetPositions(new Vector3[] { startPosition, GetMousePosition() });
            }
            else currentLineRenderer.SetPositions(new Vector3[] { startPosition, lastPosition });
        }
    }
    public ShotProperties EndDrawingLine()
    {
        if (Vector3.Distance(a,b) > lineScalar)
        {
            ShotProperties shot = new ShotProperties(currentShotAngle, currentShotPower*shotPowerModifier);
            ResetLineProperties();
            return shot;
        }
        else
        {
            ResetLineProperties();
            return null;
        }
    }
    public float CalculateAngle(Vector2 a, Vector2 b)
    {
        currentShotAngle = Mathf.Atan2(b.y-a.y,b.x-a.x) * 180 / Mathf.PI + 180;
        return currentShotAngle;
    }
    private void ResetLineProperties()
    {
        Destroy(currentLineObject);
        startPosition = Vector3.zero;
        lastPosition = new Vector3();
        currentLineObject = null;
        currentLineRenderer = null;
        currentShotPower = 0f; currentShotAngle = 0f;
    }
}

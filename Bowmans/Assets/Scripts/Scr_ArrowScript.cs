using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Scr_ArrowScript : MonoBehaviour
{
    [SerializeField] Rigidbody rb;
    [SerializeField] Scr_GameManager gm;
    [SerializeField] GameObject bloodParticles;

    bool isActive;
    [SerializeField] GameObject Model;
    [SerializeField] float spinSpeed = 5f;
    [SerializeField] float hitVelocity = 0f;

    private void Awake()
    {
        gm = GameObject.Find("Game Manager").GetComponent<Scr_GameManager>();
        Model = transform.GetChild(0).gameObject;
    }
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        isActive = true;
        StartCoroutine(Spawn());
    }
    IEnumerator Spawn()
    {
        yield return new WaitForSeconds(0.1f);
        this.GetComponentInChildren<CapsuleCollider>().enabled = true;
    }

    private void Update()
    {
        if (isActive)
        {
            transform.rotation = Quaternion.LookRotation(rb.velocity);
            if (transform.position.y < 0)
            {
                transform.position.Set(transform.position.x, transform.position.y - 10, transform.position.z);
                Deactivate();
            }
        }
    }
    private void FixedUpdate()
    {
        //Arrow Spin
        if (isActive) { if (Model != null) { Model.transform.Rotate(Vector3.up, spinSpeed); } }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (this.transform.parent == null)
        {
            Deactivate();
            if (other.gameObject.layer == 6)
            {
                this.transform.SetParent(other.gameObject.transform);
                Instantiate(bloodParticles, other.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position), Quaternion.identity);
                switch (other.name)
                {
                    case "Head":
                        Debug.Log($"Hit to {other.name} for {other.GetComponentInParent<Scr_PlayerState>().GetHP()}");
                        other.GetComponentInParent<Scr_PlayerState>().Hit(other.GetComponentInParent<Scr_PlayerState>().GetHP());
                        break;
                    case "Torso":
                        Debug.Log($"Hit to {other.name} for {hitVelocity}");
                        other.GetComponentInParent<Scr_PlayerState>().Hit(hitVelocity);
                        break;
                    case "Legs":
                        Debug.Log($"Hit to {other.name} for {hitVelocity/2}");
                        other.GetComponentInParent<Scr_PlayerState>().Hit(hitVelocity/2);
                        break;
                }
            }
        }
    }

    private void Deactivate()
    {
        hitVelocity = rb.velocity.magnitude;
        rb.velocity = Vector3.zero;
        rb.isKinematic = true;
        this.transform.localScale = new Vector3(1, 1, 1);
        this.GetComponentInChildren<Collider>().enabled = false;
        Destroy(this.GetComponentInChildren<CinemachineVirtualCamera>().gameObject);
        isActive = false;
        gm.NextTurn();
    }
}
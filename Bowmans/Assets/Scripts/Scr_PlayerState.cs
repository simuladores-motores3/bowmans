using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_PlayerState : MonoBehaviour
{
    [SerializeField] int InitialHP = 100;
    [SerializeField] float CurrentHP;
    [SerializeField] bool isAlive;
    [SerializeField] Scr_BarScript HPBar = null;

    private void Start()
    {
        ResetHP();
    }
    private void Update()
    {
        if (HPBar != null) HPBar.SetFill(CurrentHP);
    }
    public void SetBar(Scr_BarScript bar)
    {
        HPBar = bar;
        HPBar.SetMaxFill(InitialHP);
    }
    public void ResetHP()
    {
        StopAllCoroutines();
        CurrentHP = InitialHP;
        isAlive = true;
    }
    public float GetHP()
    {
        return CurrentHP;
    }

    public void Hit(float force)
    {
        StartCoroutine(TakeDamage(force));
    }
    public bool IsAlive()
    {
        return isAlive;
    }

    IEnumerator TakeDamage(float amount)
    {
        if (amount >= CurrentHP) { isAlive = false; }
        float previousHealth = CurrentHP;
        float waitFor = 0.25f / amount / 2f;
        while (CurrentHP > previousHealth - amount)
        {
            CurrentHP -= 0.1f;
            yield return new WaitForSeconds(waitFor);
            if (CurrentHP <= 0)
            {
                StopAllCoroutines();
            }
        }
    }
}

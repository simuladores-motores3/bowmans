using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_PlayersManager : MonoBehaviour
{
    [SerializeField] Scr_SpawnsManager sm;
    [SerializeField] List<Scr_PlayerState> Players = new List<Scr_PlayerState>();

    private void Awake()
    {
        sm = gameObject.AddComponent<Scr_SpawnsManager>();
    }

    public int GetPlayersCount()
    {
        return Players.Count;
    }
    public void AddPlayer(Scr_PlayerState player)
    {
        Players.Add(player);
    }
    public string GetPlayerName(int index)
    {
        return Players[index].name;
    }
    public bool CheckPlayers()
    {
        switch (Players.Count)
        {
            case 0:
                Debug.Log("Faltan 2 jugadores!");
                break;
            case 1:
                Debug.Log("Falta 1 jugador!");
                break;
            case 2:
                Debug.Log("Iniciando Juego...");
                MoveAllToSpawn();
                return true;
            default:
                Debug.Log("Esto no deber�a estar pasando xd");
                break;
        }
        return false;
    }
    public void MoveAllToSpawn()
    {
        for (int i=0; i<Players.Count; i++)
        {
            sm.MoveToSpawn(i, Players[i].transform);
            Players[i].ResetHP();
            Players[i].SetBar(sm.SpawnPoints[i].GetComponentInChildren<Scr_BarScript>());
        }
    }
    public void EnablePlayer(int index)
    {
        for (int i = 0; i < Players.Count; i++)
        {
            Players[i].GetComponent<Scr_PlayerControls>().enabled = (i != index);
        }
    }
    public void HitPlayer(int index, float hit)
    {
        Players[index].Hit(hit);
    }
    public int CheckAllPlayersAlive()
    {
        if (!Players[0].IsAlive())
        {
            return 1;
        }
        else if (!Players[1].IsAlive())
        {
            return 0;
        }
        else return -1;
    }
}

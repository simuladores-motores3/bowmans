using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;
using Cinemachine;
using TMPro;

public class Scr_GameManager : Manager
{
    [Header("Managers")]
    [SerializeField] GameObject nm;
    [SerializeField] Scr_LineManager lm;
    [SerializeField] Scr_SpawnsManager sm;
    [SerializeField] Scr_PlayersManager pm;
    [SerializeField] Scr_CameraMovement cam;

    [Header("Game Settings")]
    [SerializeField] public GameObject Arrow;
    [SerializeField] public float forceMultiplier = 3;

    [Header("Game Loop Variables")]
    public bool inGame = false;
    public bool turn = false;
    public GameObject AliveArrow;

    [Header("UI")]
    [SerializeField] TMP_Text WinText;

    
    private void Awake()
    {
        Singleton();
    }

    void Start()
    {
        lm = GetComponent<Scr_LineManager>();
        sm = GetComponent<Scr_SpawnsManager>();
        pm = GetComponent<Scr_PlayersManager>();
        nm = GameObject.Find("Network Manager");
        cam = GameObject.Find("Camera").GetComponent<Scr_CameraMovement>();
        WinText.text = "\nPresiona la Barra Espaciadora para iniciar.";
    }

    void Update()
    {
        //Start Game
        if (!inGame) { if (Input.GetKeyDown(KeyCode.Space)) { TryStartGame(); } }
        else
        {
            switch (pm.CheckAllPlayersAlive())
            {
                case 0:
                    inGame = false;
                    WinText.text = $"{pm.GetPlayerName(0)} gana!\nPresiona la Barra Espaciadora para iniciar.";
                    
                    break;
                case 1:
                    inGame = false;
                    WinText.text = $"{pm.GetPlayerName(1)} gana!\nPresiona la Barra Espaciadora para iniciar.";
                    break;
                case -1:
                    break;
            }
        }
    }

    public void TryStartGame()
    {
        GetPlayers();
        if (pm.CheckPlayers())
        {
            StartGame();
        }
    }
    private void GetPlayers()
    {
        if (pm.GetPlayersCount() < 2)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject player in players)
            {
                //If it has a player state component, add it to the list
                if (player.GetComponent<Scr_PlayerState>() != null) { pm.AddPlayer(player.GetComponent<Scr_PlayerState>()); }
                //If it doesnt, add the component and add it to the list
                else { pm.AddPlayer(player.AddComponent<Scr_PlayerState>()); }
            }
        }
    }
    private void StartGame()
    {
        WinText.text = "";
        foreach (GameObject g in GameObject.FindGameObjectsWithTag("Arrow"))
        {
            Destroy(g);
        }
        inGame = true;
        pm.EnablePlayer(turn ? 0 : 1);
    }

    public void Shoot(ShotProperties shot, GameObject arrowSpawn)
    {
        if (AliveArrow == null)
        {
            lm.enabled = false;
            AliveArrow = Instantiate(Arrow, arrowSpawn.transform.position, arrowSpawn.transform.rotation);
            Vector2 shot2D = PolarVectorToCoords(shot.Angle);
            Vector3 shot3D = new Vector3(shot2D.x * shot.Force * forceMultiplier, shot2D.y * shot.Force * forceMultiplier, 0f);
            AliveArrow.GetComponent<Rigidbody>().AddForce(shot3D, ForceMode.Impulse);
            SetCamera(AliveArrow.GetComponentInChildren<CinemachineVirtualCamera>());
        }
    }
    Vector2 PolarVectorToCoords(float angle)
    {
        float radians = angle * Mathf.Deg2Rad;
        return new Vector2(Mathf.Cos(radians), Mathf.Sin(radians));
    }
    public void NextTurn()
    {
        lm.enabled = true;
        AliveArrow = null;
        turn = !turn;
        SetCameraToCurrentPlayer();
        pm.EnablePlayer(turn ? 0 : 1);
    }
    private void SetCameraToCurrentPlayer()
    {
        if (!turn)
        {
            cam.SetCamera(sm.SpawnPoints[0].GetComponentInChildren<CinemachineVirtualCamera>());
        }
        else
        {
            cam.SetCamera(sm.SpawnPoints[1].GetComponentInChildren<CinemachineVirtualCamera>());
        }
    }

    public void SetCamera(CinemachineVirtualCamera newVCam)
    {
        cam.SetCamera(newVCam);
    }
}
